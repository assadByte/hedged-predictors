package hedgedPredictors
import org.apache.spark.mllib.linalg.{Vector,Vectors}

case class Point(val label: Double, val vector: Vector) extends Serializable {
    override def equals(o: Any): Boolean = o match {
      case that: Point => that.vector == this.vector && that.label == this.label
      case _ => false
    }
    override def hashCode: Int ={
      41 * (label + 41).toInt + vector.hashCode()
    }

    override def toString: String={
      s"($label) ($vector)"
    }
    def apply(label: Double, point: Array[Double]) = Point(label, Vectors.dense(point))
    def sameLabel(that: Point) = if(this.label == that.label) true else false
    def dist(that: Point): Double ={
      Vectors.sqdist(vector,that.vector)
    }

  }
