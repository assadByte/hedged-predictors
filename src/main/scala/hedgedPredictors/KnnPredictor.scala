package hedgedPredictors

import scala.language.postfixOps
import org.apache.spark.SparkContext._
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.HashPartitioner
import org.apache.spark.mllib.linalg.{Vector,Vectors}
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.{SparkContext,SparkConf}
import org.apache.spark.rdd.RDD

class KnnPredictor( val training: RDD[Point], val test: RDD[Point], val significance: Double, val k: Int, val calibrationWeight: Double) extends InductiveConformalPredictor[Point] with Serializable {
  val labels= training.map(_.label).distinct().collect.toSet
  val bdcastSignificance= training.context.broadcast(significance)
  val (train,calibrate)= splitTrainingSet(Array(1-calibrationWeight,calibrationWeight))
  val cachedTrain=  training.context.broadcast(train.collect)
  val cachedAlphas=  training.context.broadcast(getCalibrationAlpha.collect())
  val pValues= computePValues.persist()


  def setSignificance(newSignificance: Double): KnnPredictor={
    new KnnPredictor(training,test,newSignificance,k,calibrationWeight)
  }

  def getCalibrationAlpha: RDD[(Point,Double)]={

    val model= calibrate.map {
      point =>
      var (sameLabelPoints,diffLabelPoints)= (List[Seq[Double]](),List[Seq[Double]]())
      cachedTrain.value.foreach { that =>
        if (that.sameLabel(point) && that != point) sameLabelPoints =that.vector.toArray.toSeq :: sameLabelPoints
        else if(!that.sameLabel(point)) diffLabelPoints= that.vector.toArray.toSeq :: diffLabelPoints
      }
      
      //val sameLabelPoints= cachedData.value.filter(that=> ).map(_.vector)
      //val diffLabelPoints= cachedData.value.filter(!point.sameLabel(_)).map(_.vector)
      val sameLabelTree= KDTree.fromSeq(sameLabelPoints)(DimensionalOrdering.dimensionalOrderingForSeq[Seq[Double],Double](point.vector.size))
      val diffLabelTree=  KDTree.fromSeq(diffLabelPoints)(DimensionalOrdering.dimensionalOrderingForSeq[Seq[Double],Double](point.vector.size))
      val distPlus= sameLabelTree.findNearest(point.vector.toArray,k).toArray.map {that =>
        Vectors.sqdist(point.vector, Vectors.dense(that.toArray))
      }

      val distMinus= diffLabelTree.findNearest(point.vector.toArray,k).map {that =>
    Vectors.sqdist(point.vector, Vectors.dense(that.toArray))
      }
      (point, distPlus.sum / distMinus.sum)
    }
    model
  }


  def computePValues: RDD[(Point,Set[(Double,Double)])]={
    val labeledRddSet= for (l <- labels) yield{
      test.map {
        ex =>
        (ex,(Point(l,ex.vector)))
      }
    }

    var labelsRdd=  training.context.emptyRDD[(Point,Point)]

    labeledRddSet.foreach(rdd => labelsRdd= labelsRdd.union(rdd))

    labelsRdd= labelsRdd.persist()
    val pValues= labelsRdd.mapValues { hPoint =>
      val modelSize=  cachedAlphas.value.size
      var (sameLabelPoints,diffLabelPoints)= (List[Seq[Double]](),List[Seq[Double]]())
      cachedTrain.value.foreach { that =>
        if (that.sameLabel(hPoint) && that != hPoint) {sameLabelPoints =that.vector.toArray.toSeq :: sameLabelPoints}
        else if(!that.sameLabel(hPoint)) {diffLabelPoints= that.vector.toArray.toSeq :: diffLabelPoints
        }
      }
      // val sameLabelPoints= bdcastTraining.value.filter(that=> temp_ex.sameLabel(that) && temp_ex != that).map(_.vector)
      // val diffLabelPoints=  bdcastTraining.value.filter(that => !temp_ex.sameLabel(that)).map(_.vector)
      val sameLabelTree= KDTree.fromSeq(sameLabelPoints)(DimensionalOrdering.dimensionalOrderingForSeq[Seq[Double],Double](hPoint.vector.size))
      val diffLabelTree=  KDTree.fromSeq(diffLabelPoints)(DimensionalOrdering.dimensionalOrderingForSeq[Seq[Double],Double](hPoint.vector.size))
      val distPlus= sameLabelTree.findNearest(hPoint.vector.toArray,k).map {that =>
        Vectors.sqdist(hPoint.vector, Vectors.dense(that.toArray))
      }

      val distMinus= diffLabelTree.findNearest(hPoint.vector.toArray,k).map {that =>
                Vectors.sqdist(hPoint.vector, Vectors.dense(that.toArray))
      }
      val pValue =( cachedAlphas.value.filter(elem => elem._2 >= distPlus.sum/distMinus.sum )
        .size +1) / (modelSize +1)
        .toDouble
      (hPoint.label,pValue)
    }
    pValues.groupByKey().map(tuple => (tuple._1,tuple._2.toSet))
  }

  override def measureEfficiency: Long={
    val efficiency= predict.filter(elem => elem._2.size >1)
    efficiency.count
  }

  override  def predict: RDD[(Point,Set[Double])]= {
    pValues.map{ elem =>
      val truePoint= elem._1
      val predictionSet= elem._2.filter(it => it._2 > bdcastSignificance.value).map(_._1).toSet
      (truePoint,predictionSet)
    }
  }

  def countErrors ={
    predict.map{ case (point,predictionSet) =>
      if (!predictionSet.contains(point.label)) 1 else 0
    }.sum
  }
  def countEmpty: Long={
    predict.filter(elem => elem._2.isEmpty).count()
  }
  override def measureValidity: Double ={
    countErrors / test.count()
  }

  def computeExperiment: (Double,Double,Long,Long) ={
    val errors= countErrors
    val validity= measureValidity
    val effcy= measureEfficiency
    val empty= countEmpty
    (errors,validity,effcy,empty)
  }
}
