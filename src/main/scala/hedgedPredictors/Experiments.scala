package hedgedPredictors
import org.apache.spark.SparkContext._
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.HashPartitioner
import org.apache.spark.mllib.linalg.{Vector,Vectors}
import org.apache.spark.{SparkContext,SparkConf}
import org.apache.spark.mllib.util.MLUtils
import scala.util.Random
import java.util.Calendar
import java.io._
import org.apache.spark.rdd.RDD

object Experiment{

  //PART 1
  def calibration(data: RDD[Point], test: RDD[Point], significance: Array[Double]):Traversable[Any]={

   // val calibration= Array(0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9)
    val calibration= Array(0.2,0.5,0.7)
    val predictors= for {sig <- significance;
      size <- calibration} yield (new KnnPredictor(data,test,sig,1,size))
    
    val results= for (exp <- predictors) yield{
      val (errors,validity,effcy,empty)= exp.computeExperiment
      val significance= exp.significance
      val calibSize= exp.calibrationWeight
      ("CalibrationVsError",significance,calibSize,errors,validity,effcy,empty)
    }
    results
  }
 
  //PART 2

  def main(args: Array[String]): Unit={
    val conf= new SparkConf().setAppName("Calibration Experiment").setMaster("local[*]")
    val sc= new SparkContext(conf)
    val dataset= args(0).trim

    val significance= {try{Some(args(1).toDouble)} catch {case e: Exception => None}}
      .getOrElse(0.03)
    val k= {try{Some(args(2).toInt)} catch {case e: Exception => None}}
      .getOrElse(1)
    val calib= {try{Some(args(3).toDouble)} catch {case e: Exception => None}}
      .getOrElse(0.2)

    val testWeight= {try{Some(args(4).toDouble)} catch {case e: Exception => None}}
      .getOrElse(0.2)
    val partitions= {try{Some(args(5).toInt)} catch {case e: Exception => None}}
      .getOrElse(sc.defaultParallelism)

    val data= MLUtils.loadLibSVMFile(sc, getDataset(dataset)).map(l => Point(l.label,l.features))

    val split= data.randomSplit(Array[Double](1 - testWeight, testWeight))
    val (training,test)= (split(0),split(1))
    val (trainingSize,testSize)= (training.count,test.count)
    //    val results= calibration(training, test, Array(0.01,0.03,0.05,0.1,0.2))
    val results= calibration(training, test, Array(0.03,0.05,0.2))
    // val knn= new KnnPredictor(training: RDD[Point], test: RDD[Point], significance: Double, k: Int, calib: Double)
    // val validity= knn.measureValidity
    // val errors= knn.countErrors
    // val effccy= knn.measureEfficiency
    // val empty= knn.countEmpty
   printResults(dataset, trainingSize, testSize, results)
  }


  def oneThreeFiveNNExp(training: RDD[Point], test: RDD[Point], significance: Double, calibrationWeight: Double): Traversable[Any]={
    val nbors= Array(1,3,5)
    val results= for(k <- nbors) yield {

      val knn= new KnnPredictor(training, test, significance: Double, k, calibrationWeight)
      val (errors,validity,effcy,empty)= knn.computeExperiment
      val sig= knn.significance
      val calibSize= knn.calibrationWeight
      (s"${k}-NN",sig,calibSize,errors,validity,effcy,empty)
    }
    results
  }
   def printResults(dataset: String, trainingSize: Long,testSize: Long, pSet: Traversable[Any],significance: Double = Double.PositiveInfinity): Unit ={
    val id= Calendar.getInstance.getTime
    
    val printer= new PrintWriter(new File(s"output/$id"))
    printer.print(formatOutput)
    pSet.foreach { elem =>
      printer.print("\n "+ elem)
    }
    printer.close


    def formatOutput: String ={
      s"Experiment $id \n"+
      s"Dataset: $dataset \n"+
      s"Number of training examples: $trainingSize \n"+
      s"Number of test examples: $testSize \n"+
      s"Significance level: $significance \n"
    }
  }
  def getDataset(name: String, path: String = "src/main/scala/data/")=  path + name

  
  def formatString(string: String, char: String): String={
    string.indexOf(char) match{
      case x if x > 0 => string.drop(x+1)
      case x if x < 0 => string
    }
  } 
}
