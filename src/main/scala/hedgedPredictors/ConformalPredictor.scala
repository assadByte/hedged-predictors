package hedgedPredictors
import scala.language.postfixOps
import org.apache.spark.SparkContext._
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.HashPartitioner
import org.apache.spark.mllib.linalg.{Vector,Vectors}
import org.apache.spark.{SparkContext,SparkConf}
import org.apache.spark.rdd.RDD
import scala.util.Random
import scala.collection.mutable.ArrayBuffer
import java.io._

trait ConformalPredictor[T] extends Serializable {
  val training: RDD[T]
  val significance: Double
  def setSignificance(newError: Double): ConformalPredictor[T]
  def predict: RDD[(T,Set[Double])]
  def measureEfficiency: Long
  def measureValidity: Double
}
 
trait InductiveConformalPredictor[T] extends ConformalPredictor[T] with Serializable {

  val calibrationWeight: Double
  def splitTrainingSet(weights: Array[Double]): (RDD[T],RDD[T]) ={
    val split= training.randomSplit(weights,5789L)
    (split(0),split(1))
  }
}





// object ConformalPredictor{



  


//   // def getAlpha(points: RDD[Point], sc: SparkContext, tests: RDD[Point],labels: Set[Double]) ={
//   //   val cachedTests= sc.broadcast(tests.collect())
//   //   val cachedLabels= sc.broadcast(labels)
//   //   val k=1
//   //   val candidateAlphas= points.mapPartitions{
//   //     it =>

//   //     val alphaCandidate= for {
//   //       ex <- cachedTests.value;
//   //       y  <- cachedLabels.value
//   //     } yield {
//   //       val temp_ex= Point(y, ex.vector)
//   //       val sameLabelPoints= it.filter(that=> temp_ex.sameLabel(that) && temp_ex != that).map(_.vector)
//   //       val diffLabelPoints= it.filter(that => !temp_ex.sameLabel(that)).map(_.vector)
//   //       val sameLabelTree= KDTree.fromSeq(sameLabelPoints.toSeq)(DimensionalOrdering.dimensionalOrderingForSeq[Seq[Double],Double](temp_ex.vector.size))
//   //       val diffLabelTree=  KDTree.fromSeq(diffLabelPoints.toSeq)(DimensionalOrdering.dimensionalOrderingForSeq[Seq[Double],Double](temp_ex.vector.size))
//   //       val distPlus= sameLabelTree.findNearest(temp_ex.vector,k).toArray.map {that =>
//   //         val vc1= Vectors.dense(temp_ex.vector.toArray)
//   //         val vc2= Vectors.dense(that.toArray)
//   //         Vectors.sqdist(vc1, vc2)
//   //       }
//   //       val distMinus= diffLabelTree.findNearest(temp_ex.vector,k).toArray.map {that =>
//   //         val vc1= Vectors.dense(temp_ex.vector.toArray)
//   //         val vc2= Vectors.dense(that.toArray)
//   //         Vectors.sqdist(vc1, vc2)
//   //       }
//   //       (ex,y,distPlus,distMinus)
//   //     }
//   //    alphaCandidate.toIterator
//   //   }

//   //   }


  
//   def main(args: Array[String]): Unit={
//     val conf= new SparkConf().setAppName("Conformal Predictors").setMaster("local[*]")
//     val sc= new SparkContext(conf)
//     val k= args(0).toInt
//     val significance= args(1).toDouble


//     val dataRdd= sc.textFile("/home/nery/workspace/remote/general-purpose/machine-learning/src/main/scala/data/usps",40).map(str => str.split(" ").map(elem => formatString(elem, ":").toDouble))
//     val pointsRdd= dataRdd.map(line => Point(line(0),line.drop(1).toSeq)).persist()
//     val labels= pointsRdd.map(_.label).distinct().collect().toSet

//     val bdcastK= sc.broadcast(k)
//     val bdcastSignificance= sc.broadcast(significance)
// //    val (training,model)= getModel(pointsRdd, sc,bdcastK)

//     val accumError= sc.longAccumulator
//     val debug= sc.doubleAccumulator
//     // val bdcastModel= sc.broadcast(model.collect)
//     // val bdcastTraining= sc.broadcast(training.collect)




  
    
//     //val bdcastAlphas= sc.broadcast(model.collect)
//     val testExamples= sc.textFile("/home/nery/workspace/remote/general-purpose/machine-learning/src/main/scala/data/usps.t",30).map(str => str.split(" ").map(elem => formatString(elem, ":").toDouble)).map(array => Point(array(0),array.drop(1))).persist()


//     val labeledRddSet= for (l <- labels) yield{
//       testExamples.map {
//         ex =>
//         (ex,(Point(l,ex.vector)))
//       }
//     }
 

//     var labelsRdd= sc.emptyRDD[(Point,Point)]

//     labeledRddSet.foreach(rdd => labelsRdd= labelsRdd.union(rdd))

//     labelsRdd= labelsRdd.persist()
//     val pValues= labelsRdd.mapValues { hPoint =>
//       val modelSize= bdcastModel.value.size
//         var (sameLabelPoints,diffLabelPoints)= (List[Seq[Double]](),List[Seq[Double]]())
//          bdcastTraining.value.foreach { that =>
//           if (that.sameLabel(hPoint) && that != hPoint) {sameLabelPoints =that.vector :: sameLabelPoints}
//           else if(!that.sameLabel(hPoint)) {diffLabelPoints= that.vector :: diffLabelPoints
//           }
//         }
//         // val sameLabelPoints= bdcastTraining.value.filter(that=> temp_ex.sameLabel(that) && temp_ex != that).map(_.vector)
//         // val diffLabelPoints=  bdcastTraining.value.filter(that => !temp_ex.sameLabel(that)).map(_.vector)
//         val sameLabelTree= KDTree.fromSeq(sameLabelPoints)(DimensionalOrdering.dimensionalOrderingForSeq[Seq[Double],Double](hPoint.vector.size))
//         val diffLabelTree=  KDTree.fromSeq(diffLabelPoints)(DimensionalOrdering.dimensionalOrderingForSeq[Seq[Double],Double](hPoint.vector.size))
//         val distPlus= sameLabelTree.findNearest(hPoint.vector,bdcastK.value).map {that =>
//           val vc1= Vectors.dense(hPoint.vector.toArray)
//           val vc2= Vectors.dense(that.toArray)
//           Vectors.sqdist(vc1, vc2)
//         }

//         val distMinus= diffLabelTree.findNearest(hPoint.vector,bdcastK.value).map {that =>
//           val vc1= Vectors.dense(hPoint.vector.toArray)
//           val vc2= Vectors.dense(that.toArray)
//           Vectors.sqdist(vc1, vc2)
//         }
//       val pValue =(bdcastModel.value.filter(elem => elem._2 >= distPlus.sum/distMinus.sum ).size +1) / (modelSize +1).toDouble
//       (hPoint.label,pValue)
//     }       
//     val predictionSet= pValues.groupByKey()
//       .map{
//         elem =>
//         val truePoint= elem._1
//         val predictionSet= elem._2.filter(it => it._2 > bdcastSignificance.value).toSet
//         if (!predictionSet.map(_._1).contains(truePoint.label) && !predictionSet.isEmpty){ accumError.add(1)}
//         (truePoint,predictionSet)
//       }
//     val testSetSize= predictionSet.count()
//     // testExamples.foreach{

//     //   ex =>
//     //   val k=1
//     //   val modelSize= bdcastModel.value.size
//     //   val pValuesSet = for (y <- labels) yield {
//     //     val temp_ex= Point(y, ex.vector)
//     //     var (sameLabelPoints,diffLabelPoints)= (List[Seq[Double]](),List[Seq[Double]]())
//     //     bdcastTraining.value.foreach { that =>
//     //       if (that.sameLabel(temp_ex) && that != temp_ex) {sameLabelPoints =that.vector :: sameLabelPoints}
//     //       else if(!that.sameLabel(temp_ex)) {diffLabelPoints= that.vector :: diffLabelPoints
//     //       }
//     //     }
//     //     // val sameLabelPoints= bdcastTraining.value.filter(that=> temp_ex.sameLabel(that) && temp_ex != that).map(_.vector)
//     //     // val diffLabelPoints=  bdcastTraining.value.filter(that => !temp_ex.sameLabel(that)).map(_.vector)
//     //     val sameLabelTree= KDTree.fromSeq(sameLabelPoints)(DimensionalOrdering.dimensionalOrderingForSeq[Seq[Double],Double](temp_ex.vector.size))
//     //     val diffLabelTree=  KDTree.fromSeq(diffLabelPoints)(DimensionalOrdering.dimensionalOrderingForSeq[Seq[Double],Double](temp_ex.vector.size))
//     //     val distPlus= sameLabelTree.findNearest(temp_ex.vector,k).map {that =>
//     //       val vc1= Vectors.dense(temp_ex.vector.toArray)
//     //       val vc2= Vectors.dense(that.toArray)
//     //       Vectors.sqdist(vc1, vc2)
//     //     }

//     //     val distMinus= diffLabelTree.findNearest(temp_ex.vector,k).map {that =>
//     //       val vc1= Vectors.dense(temp_ex.vector.toArray)
//     //       val vc2= Vectors.dense(that.toArray)
//     //       Vectors.sqdist(vc1, vc2)
//     //     }

//     //     val pValue =bdcastModel.value.filter(elem => elem._2 >= distPlus.sum/distMinus.sum ).size +1 / (modelSize +1).toDouble
//     //     (y,pValue)
//     //   }
//     //   val predictionSet= pValuesSet.filter(_._2 >= significance).map(_._1)
//     //   if (!predictionSet.contains(ex.label)) accumError.add(1)

//     // }
//     printPredictions(predictionSet)
//     println("The significance is: " + significance + "\n\nThe average error of prediction is: " + accumError.value / testExamples.count().toDouble + "\n The number of mispredicted examples is :" + accumError.value + "\n Final RDD size :" + testSetSize)
    
//   }

//   def computeAlpha(data: RDD[(Int,Vector)]): Unit={
//     val dataByKey= data
//   }

//   def printPredictions(predictionSet: RDD[(Point,Set[(Double,Double)])]): Unit ={
//     val pSet= predictionSet.collect()
//     val printer= new PrintWriter(new File("Predictions.result"))
//     pSet.foreach { elem =>
//       printer.print("\n\nPoint and Prediction Set: " + elem._1 + "\n " + elem._2.toString())
//     }
//     printer.close
//   }

//   def formatString(string: String, char: String): String={
//     string.indexOf(char) match{
//       case x if x > 0 => string.drop(x+1)
//       case x if x < 0 => string
//     }
//   }
