

lazy val commonSettings= Seq(
  version := "0.1.0-SNAPSHOT",
  scalaVersion in ThisBuild := "2.11.8"
)

lazy val machineLearning= (project in file("."))
  .settings(commonSettings)

resolvers += "Sonatype OSS Releases" at "http://oss.sonatype.org/content/repositories/releases/"

libraryDependencies += "com.thesamet" %% "kdtree" % "1.0.4"
libraryDependencies += "org.apache.hadoop" % "hadoop-client" % "2.3.0"
libraryDependencies += "org.apache.spark" % "spark-core_2.11" % "2.3.0"
// libraryDependencies += "org.apache.spark" % "spark-core_2.10" % "2.3.0"
libraryDependencies += "org.apache.spark" % "spark-mllib_2.11" % "2.3.0"
libraryDependencies += "org.apache.spark" % "spark-streaming_2.11" % "2.3.0"
//resolvers += Resolver.sonatypeRepo("public")
// assemblyMergeStrategy in assembly := {
//   case PathList("org","aopalliance", xs @ _*) => MergeStrategy.last
//   case PathList("javax", "inject", xs @ _*) => MergeStrategy.last
//   case PathList("javax", "servlet", xs @ _*) => MergeStrategy.last
//   case PathList("javax", "activation", xs @ _*) => MergeStrategy.last
//   case PathList("org", "apache", xs @ _*) => MergeStrategy.last
//   case PathList("com", "google", xs @ _*) => MergeStrategy.last
//   case PathList("com", "esotericsoftware", xs @ _*) => MergeStrategy.last
//   case PathList("com", "codahale", xs @ _*) => MergeStrategy.last
//   case PathList("com", "yammer", xs @ _*) => MergeStrategy.last
//   case "about.html" => MergeStrategy.rename
//   case "META-INF/ECLIPSEF.RSA" => MergeStrategy.last
//   case "META-INF/mailcap" => MergeStrategy.last
//   case "META-INF/mimetypes.default" => MergeStrategy.last
//   case "plugin.properties" => MergeStrategy.last
//   case "log4j.properties" => MergeStrategy.last
//   case "overview.html" => MergeStrategy.last  // Added this for 2.1.0 I think
//   case x =>
//     val oldStrategy = (assemblyMergeStrategy in assembly).value
//     oldStrategy(x)
// }

// /* including scala bloats your assembly jar unnecessarily, and may interfere with 
//    spark runtime */
// assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false) 
// assemblyJarName in assembly := "spark-mllib-test.jar"

// /* you need to be able to undo the "provided" annotation on the deps when running your spark 
//    programs locally i.e. from sbt; this bit reincludes the full classpaths in the compile and run tasks. */
// fullClasspath in Runtime := (fullClasspath in (Compile, run)).value 

 // assemblyMergeStrategy in machineLearning := {
 //  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
 //  case x => MergeStrategy.first
 // }
ensimeIgnoreScalaMismatch in LocalProject("machinelearning"):= true
